#!/bin/bash

clear

printf "EDITOR DE PROMPT DO USER\n\n"

printf "Escolhendo a cor...\n[0] branco\n[1] vermelho\n[2] laranja\n[3] amarelo\n[4] verde\n[5] azul\n[6] indigo\n[7] violeta\n"

read cor

printf "\nEscolhendo informações adicionais... \n[0] User\n[1] Caminho\n[2] User+Data\n[3] User+Caminho\n[4] User+Data+Caminho\n[5] User+Caminho+Data\n"

read info

test "$cor" = "0" && cor="\[\e[97m\]" ; test "$cor" = "1" && cor="\[\e[31m\]" ; test "$cor" = "2" && cor="\[\e[91m\]" ; test "$cor" = "3" && cor="\[\e[33m\]"; test "$cor" = "4" && cor="\[\e[32m\]" ; test "$cor" = "5" && cor="\[\e[34m\]" ; test "$cor" = "6" && cor="\[\e[94m\]" ; test "$cor" = "7" && cor="\[\e[35m\]" 

test "$info" = "0" && info="\u" ; test "$info" = "1" && info="\w" ; test "$info" = "2" && info="\u:\D{%d-%m-%Y}" ; test "$info" = "3" && info="\u\w" ; test "$info" = "4" && info="\u:\D{%d-%m-%Y}\w" ; test "$info" = "5" && info="\u\w:\D{%d-%m-%Y}"

test -e "ps1dbkp" || echo "$PS1" > ps1dbkp

cop="$cor$info\[\e[97m\]$ "

echo "$cop"

printf "\nAplicar personalização escolhida?\n[s] Sim\n[n] Não\n[d] Voltar ao padrao original...\n"

read esc

test "$esc" = "s" && PS1=$cop ; test "$esc" = "d" && PS1=$(cat ps1dbkp)
