#!/bin/bash

printf "CALCULADORAZINHA\n\nUSE:\n+ para somar;\n- para subtrair;\n* para multiplicar;\n/ para dividir; e\n// para tirar a raiz quadrada\n\n"

printf "Digite um valor...\n"

read var1

printf "Digite a operação...\n"

read op

test "$op" != "//" && printf "Digite o segundo valor...\n" && read var2

test "$op" != "//" && resultado=$(python3 -c "print (int($var1) $op int($var2))") || resultado=$(python3 -c "from math import sqrt; print (sqrt($var1))")

printf "Resultado = $resultado\n"
